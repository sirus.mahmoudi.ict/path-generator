import { CheckPointOption } from "../../checkpoint/checkpoint-options";
import { OptionDirection } from "./option-direction";
import { PathSizing } from "../../path/path-sizing";
import { Status } from "../../status";

export class LeftOptionDirection extends OptionDirection {
    constructor() {
        super();
    }

    /**
     * Return the properties of the starting point
     * @param {PathSizing} path_sizing
     * Width and height of the checkpoint without margin
     * @param {number} radius
     * Checkpoints radius
     */
    getStartOptions(path_sizing: PathSizing, radius: number) {
        const startOption = { label: "Begin", type: "start", isRandom: false, x: path_sizing.width - (radius * 2), y: (path_sizing.height / 2) + radius, childrens: [], status: Status.FINISHED };

        return startOption;
    }

    /**
     * Return the path length in pixels from starting point to last
     * @param {any} first_point
     * @param {any} last_point
     */
    getPathLength(first_point: any, last_point: any): PathSizing {
        const pathLength = {
            width: first_point.x - last_point.x,
            height: last_point.y - first_point.y
        };

        return pathLength;
    }
}