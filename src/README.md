# rand-svg-path-generator

rand-svg-path-generator a TypeScript tool to randomly generate a svg composed of a path and checkpoints




## Installation

Use the node package manager ([npm](https://www.npmjs.com/package/@dev-to-be-curious/rand-svg-path-generator)) to install foobar.

```bash
npm i @dev-to-be-curious/rand-svg-path-generator
```



## Usage

There are two ways to generate your map: either by integrating your checkpoint data directly into the map generation settings, or through an api.


### Basic usage

```js
// Import module
import { Map } from "@dev-to-be-curious/rand-svg-path-generator";

// Setup general settings containing the checkpoints data
let settings = {
    width: 0, // set 0 for max screen width
    height: 0, // set 0 for max screen height
    margin: {
        top: 20,
        right: 30,
        bottom: 30,
        left: 30
    },
    delta_y: 150, // Max variation of y position between two checkpoints
    checkpoints: {
        radius: 15, // Default radius of a checkpoint (objectives will be half size)
        randomizer: {
            maxAlpha: 60, // Max angle value for y position generation using trigonometry
            distance: 150 // Distance between two checkpoints
        }
    },
    apiUrl: '',
    data: [{
        "type": "start",
        "label": "Start",
        "status": "FINISHED" // Status can be: LOCKED | TODO | CURRENT | FINISHED
    },
    {
        "label": "Customer-focused",
        "status": "FINISHED",
        "childrens": [{
                "label": "Public-key",
                "type": "children",
                "status": "FINISHED"
            },
            {
                "label": "Automated",
                "type": "children",
                "status": "FINISHED"
            }
        ]
    },
    {
        "label": "Distributed",
        "status": "TODO",
        "childrens": [{
                "label": "Configurable",
                "type": "children",
                "status": "FINISHED"
            },
            {
                "label": "Compatible",
                "type": "children",
                "status": "FINISHED"
            },
            {
                "label": "Persistent",
                "type": "children",
                "status": "CURRENT"
            }
        ]
    },
    {
        "type": "end",
        "label": "Stop",
        "status": "LOCKED"
    }
]
};

// Instantiate a new Map
const map = new Map(settings);

// Example of default style manipulation
// You can also manipulate style throught css
map.path.styles.checkpoints.end.fill = 'orange';


// Generate map by api
map.generateMap();
```

### Warning !
If you use status please add the following svg in your assets folder : ***locked.svg***, ***todo.svg***, ***current.svg*** and ***finished.svg***.

Set svg in your assets folder at **/@dev-to-be-curious/rspg/**.

### From API
```js
// Import module
import { Map } from "@dev-to-be-curious/rand-svg-path-generator";

// Setup general settings
let settings = {
    width: 0,
    height: 0,
    margin: {
        top: 20,
        right: 30,
        bottom: 30,
        left: 30
    },
    delta_y: 150,
    checkpoints: {
        radius: 15,
        randomizer: {
            maxAlpha: 60,
            distance: 150
        }
    },
    apiUrl: 'http://127.0.0.1:3000',
    data: []
};

// Instantiate a new Map
const map = new Map(settings);

// Example of default style manipulation
// You can also manipulate style throught css
map.path.styles.checkpoints.end.fill = 'orange';


// Generate map by api
map.generateMapByApi(settings.apiUrl);
```



## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.



## License

 > Copyright © 01-18-2021, Félicien Fouillet - DevToBeCurious
 > 
 > Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subje-ct to the following conditions:
 >
 > The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 >
 > The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders X be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
 >
 > Except as contained in this notice, the name of Félicien Fouillet or DevToBeCurious shall not be used in advertising or otherwise to promote the sale, use or other dealings in this Software without prior written their authorization.
