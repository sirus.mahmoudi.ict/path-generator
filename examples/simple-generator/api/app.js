const http = require('http');

// All CP to generate
const testData = [{
        "type": "start",
        "label": "Start",
        "status": "FINISHED"
    },
    {
        "label": "Customer-focused",
        "status": "TODO",
        "childrens": [{
                "label": "Public-key",
                "type": "children",
                "status": "FINISHED"
            },
            {
                "label": "Automated",
                "type": "children",
                "status": "PAUSED"
            }
        ]
    },
    {
        "label": "Distributed",
        "status": "TODO",
        "childrens": [{
                "label": "Configurable",
                "type": "children",
                "status": "FINISHED"
            },
            {
                "label": "Compatible",
                "type": "children",
                "status": "FINISHED"
            },
            {
                "label": "Persistent",
                "type": "children",
                "status": "RUNNING"
            }
        ]
    },
    {
        "type": "end",
        "label": "Stop",
        "status": "LOCKED"
    }
]

const server = http.createServer(function(req, res) {
    res.writeHead(200, {
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*"
    });
    res.end(JSON.stringify(testData));
})

// Lancer le serveur
server.listen(3000);